#!/bin/sh
#Author: Simon Podlesny (xpodle01)

#Globalne premenne
pocitadlo="0"
DD="0"
begin=$(pwd)

#vstup: zoznam_adresarov
pocet_adresarov() {
	cd -- "$begin"

	pocet=$(echo "$1" | wc -l | sed -e 's/^[ \t]*//')
	
	ND=$(echo "$pocet" | bc | sed -e 's/^[ \t]*//')

	if [ -z "$ND" ]; then
		ND="1"
	fi
}

#vstup: cesta
adresarova_hlbka() {
 	#DD=$(find $1 -type d -printf '%d:%p\n' | sort -n | tail -1 | grep -ohE "[0-9]{1,99}:" | grep -ohE "[0-9]{1,99}")
 	
	pocitadlo=$(echo "$pocitadlo + 1" | bc)

    cd -- "$1"

	for dir in *
	    do
	      if [ -d "$dir" ]
	      then
	        adresarova_hlbka "$dir"
	      fi
	    done

	if [ "$pocitadlo" -ge "$DD" ];	then
	    DD="$pocitadlo" 
	    #maxdir="$PWD"
	fi

	pocitadlo=$(echo "$pocitadlo - 1" | bc)
	if [ "$pocitadlo" -ne "0" ];	then
		#echo  "$pocitadlo"
		cd ..
	else
		if [ -z "$DD" ]; then
			DD="1"
		fi
	fi

}

#vstup: zoznam_suborov
priemer_a_pocet_suborov() { #ocakava vystup z pocet_adresarov()
    cd -- "$begin"
	NF=$(echo "$1" | wc -l | sed -e 's/^[ \t]*//')
	#echo "NF: $NF"
	#echo "ND: $ND"
	AF=$(echo "$NF / $ND" | bc | sed -e 's/^[ \t]*//')
	#echo "AF: $AF"

	if [ -z "$NF" ]; then
		NF="0"
	fi

	if [ -z "$AF" ]; then
		AF="0"
	fi
	#echo "$NF"
}

#vstup: zoznam_suborov
najvacsi_subor() {
	#LF=$(find $1 -type f -printf "%s %p\n" | sort -rn | head -1 | grep -ohE "[0-9]{1,99} ")
	cd -- "$begin"
	velkosti=$(echo "$1" | while read -r line; do echo "line: $line"; wc -c "$line"; done)

	#echo "$velkosti" | sort -rn | sed -n '1p'
	#cho "$velkosti" | sort -rn | sed -n '1p' | sed '/^$/d' | grep -ohE "[0-9]{1,99}\s"
	#echo "$velkosti" | sort -rn | sed -n '1p' | sed -e 's/^[ \t]*//' | grep -ohE "^[0-9]{1,99}"
	LF=$(echo "$velkosti" | sort -rn | sed -n '1p' | sed -e 's/^[ \t]*//' | grep -ohE "^[0-9]{1,99}" ) #treba zoznam, nie cestu
	
	if [ -z "$LF" -o "$LF" -eq "0" ]; then
		LF="N/A"
	fi
}

#vstup: zoznam_suborov
priemerna_velkost_suborov() {
	cd -- "$begin"
	#celkova_velkost=$(find $1 -type f | xargs -i stat --printf="%s\n" "{}" | paste -sd+ | bc)
	if [ -z "$NF" -o "$NF" -eq "0" ]; then
		AS="N/A"
	else
		velkosti=$(echo "$1" | while read -r line; do wc -c "$line"; done)
		celkova_velkost=$(echo "$velkosti" | sort -rn | sed -e 's/^[ \t]*//' | grep -ohE "^[0-9]{1,99}" | paste -sd+ - | bc) #treba zoznam, nie cestu!
		AS=$(echo "$celkova_velkost / $NF" | bc | sed -e 's/^[ \t]*//')

		if [ -z "$AS" ]; then
			AS="0"
		fi
	fi
}

#vstup: zoznam_suborov
median() {
	cd -- "$begin"
	if [ -z "$NF" ]||[ "$NF" -eq "0" ]; then
		MS="N/A"
	else
		#sortnuty_zoznam=$(find $1 -type f | xargs -i stat --printf="%s\n" "{}" | sort)
		velkosti=`echo "$1" | while read -r line; do	wc -c "$line"; done`
		sortnuty_zoznam=$(echo "$velkosti" | sed -e 's/^[ \t]*//' | grep -ohE "^[0-9]{1,99}" | sort -rn)
		pocet=$(echo "$sortnuty_zoznam" | wc -l | sed -e 's/^[ \t]*//')
		if [ `echo "$pocet % 2" | bc` -eq "0" ]; then
			#echo "pripona: $EXT"

			cislo1=$(echo "$pocet/2" | bc) #posunute o +1 lebo sed
			cislo2=$(echo "($pocet/2)+1" | bc)

			#echo "cislo1: $cislo1"
			#echo "cislo2: $cislo2"
			#echo "sortnuty_zoznam: $sortnuty_zoznam"
			#echo "$sortnuty_zoznam" | sed -n "$cislo2"p

			hodnota1=$(echo "$sortnuty_zoznam" | sed -n "$cislo1"p)
			hodnota2=$(echo "$sortnuty_zoznam" | sed -n "$cislo2"p)

			#echo "hodnota1: $hodnota1"
			#echo "hodnota2: $hodnota2"
			#echo "($hodnota1 + $hodnota2) / 2"

			MS=$(echo "($hodnota1 + $hodnota2) / 2" | bc | sed -e 's/^[ \t]*//')

		elif [ "$pocet" -eq "1" ]; then
			MS=$(echo "$sortnuty_zoznam" | sed -n "$pocet"p)
		else
			pocet=$(echo "($pocet/2)+1" | bc)
			MS=$(echo "$sortnuty_zoznam" | sed -n "$pocet"p)		

		fi

		if [ -z "$MS" ]; then
			MS="0"
		fi
	fi
}

#vstup: zoznam_suborov
pripony() {
	cd -- "$begin"
	tmp_zoznam=`echo "$1" | grep -oE '[^\/][.][[:alnum:]]+$' | sed 's/.[.]//'`
	zoznam_pripon=`echo "$tmp_zoznam" | sort -u`
	EL=`echo "$zoznam_pripon" | paste -sd, -`
	
	if [ -z "$EL" ]; then
		EL=""
	fi
}

#naplni vstupne premenne regex a cesta na zaklade spustenia shell scriptu
if [ "$1" = "-i" ]; then #zadany regex
	if [ $# -eq "2" ]; then #zadany len regex bez cesty
		regex="$2"
		cesta="$PWD"
	elif [ $# -eq "3" ]; then #zadany regex so specifickou cestou
		regex="$2"
		cesta="$3"
	else #pre viacej parametrov error
		echo "TMI" >&2
		exit 42
	fi
else
	if [ $# -eq "1" ]; then #pokial je zadana cesta
	    regex="$."
		cesta="$1"
	elif [ $# -eq "0" ]; then #bez cesty
		regex="$."
		cesta="$PWD"
	else
		echo "TMI" >&2
		exit 42
	fi
fi

#vytvori zoznam suborov s ktorymi sa ma pocitat
#premenne:
#			$zoznam_komplet
#			$zoznam_suborov
#			$zoznam_adresarov

zoznam_komplet=`find $cesta`
if [ "$?" -ne "0" ]; then 
	echo "2+2=42" >&2
	exit 42
else
	zoznam_komplet=`echo "$zoznam_komplet" | grep -vE $regex  | paste -sd "\n" -`
fi

zoznam_suborov=`find $cesta -type f`
if [ "$?" -ne "0" ]; then 
	echo "2+2=42" >&2
	exit 42
else
	zoznam_suborov=`echo "$zoznam_suborov" | grep -vE $regex  | paste -sd "\n" -`
fi

zoznam_adresarov=`find $cesta -type d`
if [ "$?" -ne "0" ]; then 
	echo "2+2=42" >&2
	exit 42
else
	zoznam_adresarov=`echo "$zoznam_adresarov" | grep -vE $regex  | paste -sd "\n" -`
fi

echo "nic" | grep -ohE "$regex"
if [ "$?" -ne "0" -a "$?" -ne 1 ]; then
	echo "$?"
	echo "$regex"
	echo "2+2=9" >&2
	exit 42
fi

#zatial prototyp vystupu
#clear
echo "Root directory: $cesta"

pocet_adresarov "$zoznam_adresarov"
echo "Directories: $ND"

#echo "$cesta"
adresarova_hlbka "$cesta"
echo "Max depth: $DD"

if [ -z "$zoznam_suborov" ]; then
	echo "Average no. of files: 0"
	echo "All files: 0"
	echo "  Largest file: N/A"
	echo "  Average file size: 0"
	echo "  File size median: 0"
else
	priemer_a_pocet_suborov "$zoznam_suborov"
	echo "Average no. of files: $AF"
	echo "All files: $NF"

	najvacsi_subor "$zoznam_suborov"
	echo "  Largest file: $LF"

	priemerna_velkost_suborov "$zoznam_suborov"
	echo "  Average file size: $AS"

	median "$zoznam_suborov"
	echo "  File size median: $MS"

	pripony "$zoznam_suborov"
	echo "File extensions: $EL"
fi

zoznam_pripon=$(echo "$EL" | sed $'s/,/\\\n/g')

if [ -z "$zoznam_pripon" ]; then
	echo "File extensions:"
else
	#echo "zoznam_pripon:$zoznam_pripon"
	echo "$zoznam_pripon" | while read -r line
	do
		EXT="$line"
		#echo "EXT: $EXT"

		tmp_zoznam_suborov=$(echo "$zoznam_suborov" | grep -hE "[.]$line"  | paste -sd "\n" -) #blby operator pri grep
    	#echo "tmp_zoznam_suborov: $tmp_zoznam_suborov"
		NEXT=$(echo "$tmp_zoznam_suborov" | wc -l | sed -e 's/^[ \t]*//')
		#echo "NEXT: $NEXT"

		#prepisuje sa pôvodna hodnota $LF!
		najvacsi_subor "$tmp_zoznam_suborov"
		LEXT="$LF"
		
		#prepisuje sa pôvodna hodnota $AS, $NF, $AF!
		priemer_a_pocet_suborov "$tmp_zoznam_suborov"
		priemerna_velkost_suborov "$tmp_zoznam_suborov"
		AEXT="$AS"

		median "$tmp_zoznam_suborov"
		MEXT="$MS"

		echo "Files .$EXT: $NEXT"
		echo "  Largest file .$EXT: $LEXT"
		echo "  Average file size .$EXT: $AEXT"
		echo "  File size median .$EXT: $MEXT"

	done

fi
